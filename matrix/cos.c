#include <stdio.h>
#include <stdlib.h>

double powi(double x,int n){
  double r = 1;
  for(int i = 0; i < n; ++i)
    r *= x;
  return r;
}

int fact(int n){
  if (n==0)
    return 1;
  else
    return  n*fact(n-1);
}

int main(int argc,char  *argv[]){
  float x = atof(argv[1]); double y = 0;
  int x_int = (int)x;
  x_int %= 4;
  for(int i=0; i < 10; i++){
    y += powi(-1,i)*powi(x,2*i)/fact(2*i);
      }
  printf("%f",y);return y;
}



