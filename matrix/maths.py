def carre(x):
    # Retourne le carre du nombre
    return x*x

def cube(x):
    # Retourne le cube du nombre
    return x*x*x

def bicarre(x):
    return carre(x)*carre(x)

def fact(n):
    if n == 0 or n == 1:
        return 1
    else:
        return n*fact(n-1)

def cosinus(x):
    x = x%(2*3.14)
    y = 0
    for i in range(20):
        y += (-1)**i*x**(2*i)/fact(2*i)
    return y

print(cosinus(6.27))
