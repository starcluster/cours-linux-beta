#!/usr/bin/python3

a, b, c = map(int, input().strip().split())

print(sorted([a,b,c]))
